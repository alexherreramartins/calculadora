package br.com.calculadora.services;

import br.com.calculadora.dtos.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){
        int resultado=0;

        for (int numero : calculadora.getNumeros()){
            resultado += numero;
        }

        return new RespostaDTO(Double.valueOf(resultado));
    }

    public RespostaDTO subtrair(Calculadora calculadora){
        int resultado = calculadora.getNumeros().get(0);

        calculadora.getNumeros().remove(0);

        for (int numero : calculadora.getNumeros()){
            resultado -= numero;
        }

        return new RespostaDTO(Double.valueOf(resultado));
    }

    public RespostaDTO multiplicar(Calculadora calculadora){
        int resultado=1;

        for (int numero : calculadora.getNumeros()){
            resultado = resultado * numero;
        }

        return new RespostaDTO(Double.valueOf(resultado));
    }

    public RespostaDTO dividir(Calculadora calculadora){

        double resultado = (double) calculadora.getNumeros().get(0) / (double) calculadora.getNumeros().get(1);
        return new RespostaDTO(resultado);
    }

}
