package br.com.calculadora.controllers;

import br.com.calculadora.dtos.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {


        if (calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "é necessário pelo menos 2 números");
        }

        for (Integer i : calculadora.getNumeros()) {
            if (Math.signum(i) < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só são permitidas operações com numeros naturais");
            }
        }

        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "é necessário pelo menos 2 números");
        }

        for (Integer i : calculadora.getNumeros()) {
            if (Math.signum(i) < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só são permitidas operações com numeros naturais");
            }
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "é necessário pelo menos 2 números");
        }

        for (Integer i : calculadora.getNumeros()) {
            if (Math.signum(i) < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só são permitidas operações com numeros naturais");
            }
        }

        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1 || calculadora.getNumeros().size() > 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só é possivel efetuar a operação com 2 números");
        }

        for (Integer i : calculadora.getNumeros()) {
            if (Math.signum(i) < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Só são permitidas operações com numeros naturais");
            }
        }

        if (calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Dividendo deve ser maior que o divisor");
        }

        return calculadoraService.dividir(calculadora);
    }
}
